package DAY2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DemoUpdate {
    private static int result;

	public static void main(String[] args) {

        Connection con = null;
        Statement stmt = null;

        System.out.println("Enter Student Id and New Fees");
        Scanner scan = new Scanner(System.in);
        int stuId = scan.nextInt();
        double fees = scan.nextDouble();
        System.out.println();

        String url = "jdbc:mysql://localhost:3306/fsd57";

        // Corrected variable names and SQL query
        String updateQuery = "UPDATE student SET Fees = " + fees + " WHERE StudentId = " + stuId;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, "root", "root");

            stmt = con.createStatement();

			if (result > 0) {
                System.out.println("Student Record Updated");
            } else {
                System.out.println("Failed to Update the Student Record!!!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
